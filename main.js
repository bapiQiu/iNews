import App from './App'
import params from './utils/params.js'
import request from './utils/request.js'
import utils from './utils/utils.js'
import noti from './utils/notification.js'

Vue.prototype.$params = params
Vue.prototype.$request = request
Vue.prototype.$utils = utils
Vue.prototype.$noti = noti

// #ifndef VUE3
import Vue from 'vue'
Vue.config.productionTip = false
App.mpType = 'app'
const app = new Vue({
	...App
})
app.$mount()
// #endif

// #ifdef VUE3
import {
	createSSRApp
} from 'vue'
export function createApp() {
	const app = createSSRApp(App)
	return {
		app
	}
}
// #endif
