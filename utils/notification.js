let notices = []

//注册通知index,通知名称name（字符串）通知（注册）对象（组件、页面对象），接到通知后要执行的函数


/**
 * @param {String} name 通知的名称或类型，建议写在专门的参数文件中
 * @param {Object} page 注册通知的对象，一般为页面，也可能是组件
 * @param {Function} selector 通知执行的函数，如何处理这个通知
 */
function add(name, page, selector) {
 if (!name || !page || !selector) {
  console.log("请参考全部参数");
  return
 }
 let notice = {
  name: name,
  page: page,
  selector: selector
 }
 notices.push(notice)
}

//移除通知
/**
 * @param {String} name 要移除的通知名称
 * @param {Object} page 移除的通知所在页面或组件对象
 */
function remove(name, page) {
 for (let i = 0; i < notices.length; i++) {
  let notice = notices[i]
  if (notice.name === name) {
   if (notice.page === page) {
    notices.splice(i, 1)
    return
   }
  }
 }
}
//发送通知blog
/**
 * @param {String} name 发送通知的名称（类型）
 * @param {Object} args 要携带的参数
 */
function post(name, args) {
 if (notices.length == 0) {
  console.log("notification;目前没有任何通知");
  return
 }
 for (let i = 0; i < notices.length; i++) {
  let notice = notices[i]
  if (notice.name == name) {
   notice.selector(args)
  }
 }
}
module.exports = {
 add,
 remove,
 post
}
