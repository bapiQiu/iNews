/**
 *  @param {String} url 
 * @param {JSON} header
 * @param {funComplete} funComplete
 */
function getwithParams(url, header, funSuccess, funComplete = () => {}) {
	uni.showLoading({
		title: "加载中...",
		mask: false
	});
	uni.request({
		url: url,
		method: 'GET',
		header: header,
		sslVerify: false,
		success: res => {
			funSuccess(res)
		},
		fail:err => {
			console.log(err)
		},
		complete: () => {
			uni.hideLoading()
			funComplete()
		}
	});
}

function get(url, funSuccess, funComplete = () => {}) {
	uni.showLoading({
		title: "加载中...",
		mask: false
	});
	uni.request({
		url: url,
		method: 'GET',
		header: {
			"content-type": "application/json;charset=UTF-8"
		},
		sslVerify: false,
		success: res => {
			funSuccess(res)
		},
		fail: err => {
			console.log(err)
		},
		complete: () => {
			uni.hideLoading()
			funComplete()
		}
	});
}

function postParams(url, data, funSuccess, funComplete = () => {}) {
	uni.showLoading({
		title: "提交...",
		mask: false
	});
	uni.request({
		url: url,
		header: {
			"accept": "application/json,*/*;q=0.1",
			"content-type": "application/x-www-form-urlencoded;charset=UTF-8"
		},
		method: 'POST',
		data: data,
		success: res => {
			funSuccess(res)
		},
		fail: err => {
			console.log(err);
		},
		complete: () => {
			uni.hideLoading()
			funComplete()
		}
	});

}
function post(url,data,funSuccess,funComplete=()=>{}){
	uni.showLoading({
		title:'提交..',
		mask:false
	});
	uni.request({
		url:url,
		header:{
			"content-type": "application/json;charset=UTF-8"
		},
		method:'POST',
		data:data,
		sslVerify:false,
		success: res =>{
			funSuccess(res.data)
		},
		fail: err => {
			console.log(err)
		},
		complete: () => {
			uni.hideLoading()
			funComplete()
		}
	});
}

function postFile(url,param,filePath,data,funSuccess,funComplete=()=>{}){
	uni.showLoading({
		title:'提交..',
		mask:false
	});
	uni.uploadFile({
		url:url,
		sslVerify:false,
		filePath:filePath,
		name:param,
		formData:data,
		success: res =>{
			funSuccess(res)
		},
		fail(err){
			console.log(err)
		},
		complete :()=>  {
			uni.hideLoading()
			funComplete()
		}
	});
}
/**
 * 以同步写法实现异步请求，在嵌套请求时
 */
function request(url,data={},method='GET'){
	return new Promise((resovle,reject)=>{
		uni.request({
			url,
			method,
			data,
			success:res=>{
				resovle(res.data)
			},
			fail:err=>{
				reject(err)
			}
		});
	})
}
function deleteParams(url,data,funSuccess){
	uni.request({
		url,
		method:'DELETE',
		data,
		header:{
			"content-type": "application/x-www-form-urlencoded;charset=UTF-8"
		},
		success:res=>{
			funSuccess(res.data)
		},
		fail:err=>{
			console.log(err)
		},
		complete: () => {}
	});
}
module.exports = {
	getwithParams,
	get,
	postParams,
	post,
	postFile,
	request,
	deleteParams
}
