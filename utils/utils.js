const formatTime = date => {
	let year = date.getFullYear()
	let month = date.getMonth() + 1
	let day = date.getDate()
	let hour = date.getHours()
	let minute = date.getMinutes()
	let Second = date.getSeconds()
	return `${[year,month,day].map(formatNumber).join('-')} ${[hour,minute,Second].map(formatNumber).join(':')}`
}
const formatNumber = n => {
	n = n.toString()
	return n[1] ? n : `0${n}`
}

function checkLength(field, minLen, name) {
	if (field.length >= minLen) {
		return true
	}
	let message = `${name}不能少于${minLen}个字符`
	if (field.length == 0) {
		message = `${name}不能为空`
	}
	uni.showToast({
		title: message,
		icon: 'none'
	});
	return false
}
function ellipsis(value,len){
	if(value&&value.length>len){
		return `${value.substr(0,len -3)}...`
	}
	return value
}
function distinct(array){
	let result=[]
	let obj={}
	for(let i of array){
		if(!obj[i.toLowerCase()]){
			result.push(i)
			obj[i.toLowerCase()]=1
		}
	}
	return result
}
module.exports = {
	formatTime,
	checkLength,
	ellipsis,
	distinct
}
